<?php
return call_user_func(function () {
    Yii::setPathOfAlias('lib', dirname(__FILE__) . '/../../lib');
    Yii::setPathOfAlias('shared', dirname(__FILE__) . '/../../shared');

    $config = array(
        'basePath' => dirname(__FILE__) . '/..',
        'name' => 'My Web Application',

        'preload' => array('log'),

        'import' => array(
            'application.components.*',
            'shared.*',
            'shared.models.*',
            'shared.helpers.*'
        ),

        'timeZone' => 'Europe/Moscow',
        'modules' => array(),

        'components' => array(
            'request' => array(
                'enableCsrfValidation' => true,
                'enableCookieValidation' => true,
                'csrfTokenName' => 'csrf_token',
                'csrfCookie' => array(
                    'httpOnly' => true
                )
            ),
            'authManager' => array(
                'class' => 'PhpAuthManager',
                'authFile' => dirname(__FILE__) . '/auth.php',
                'defaultRoles' => array(
                    'guest'
                ),
                'showErrors' => YII_DEBUG
            ),
            'errorHandler' => array(
                'errorAction' => 'site/error',
            ),
            'db' => array(
                'connectionString' => 'mysql:host=127.0.0.1;dbname=labtask',
                'emulatePrepare' => true,
                'username' => 'root',
                'password' => '',
                'charset' => 'utf8',
                'schemaCachingDuration' => 1000,
                'enableProfiling' => YII_DEBUG,
                'enableParamLogging' => YII_DEBUG,
            ),
            'user' => array(
                'allowAutoLogin' => true,
                'class' => 'WebUser',
                'loginUrl' => array('auth/index'),
            ),
            'viewRenderer' => array(
                'class' => 'lib.ETwigViewRenderer',
                'fileExtension' => '.twig',
                'options' => array(
                    'autoescape' => true,
                ),
                'globals' => array(
                    'html' => 'CHtml'
                ),
                'functions' => array(
                    'rot13' => 'str_rot13',
                    'humanFriendlyDate' => 'StringsHelper::getHumanFriendlyDate',
                ),
                'filters' => array(
                    'jencode' => 'CJSON::encode',
                    'var_dump' => 'var_dump',
                    'nl2p' => 'StringsHelper::nl2p',
                )
            ),
            'urlManager' => array(
                'urlFormat' => 'path',
                'showScriptName' => false,
                'caseSensitive' => false,
                'rules' => array(
                    'gii' => 'gii',
                    'gii/<controller:\w+>' => 'gii/<controller>',
                    'gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',

                    '/' => 'site/index',
                    '/auth' => 'auth/index',

                    '<controller:\w+>/add/' => '<controller>/put',
                    '<controller:\w+>/<id:\d+>' => '<controller>/view',
                    '<controller:\w+>/edit/<id:\d+>' => '<controller>/put',

                    '<controller:\w+>/' => '<controller>/index',
                    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                    '<controller:\w+>/<action:\w+>' => '<controller>/<action>'
                )
            ),
            'log' => array(
                'class' => 'CLogRouter'
            )
        ),
        'params' => array(
            'urls' => array(
                'static' => '/static/'
            )
        ),
    );

    $packageJsonFile = dirname(__FILE__) . '/../../package.json';
    if (file_exists($packageJsonFile)) {
        $packageJsonInfo = @file_get_contents($packageJsonFile);
        $packageJsonInfo = @json_decode($packageJsonInfo);
        $config['params']['version'] = CHtml::value($packageJsonInfo, 'version', 'unknown');
    }

    $env = CHtml::value($_SERVER, 'ENV', 'default');
    $overrideConfigFile = dirname(__FILE__) . "/local_main/{$env}.php";
    if (file_exists($overrideConfigFile)) {
        $config = array_replace_recursive($config, require($overrideConfigFile));
    }

    if (YII_DEBUG) {
        $config['modules']['gii'] = array(
            'class' => 'system.gii.GiiModule',
            'password' => '123',
            'ipFilters' => array('127.0.0.1', '::1'),
        );
    }

    return $config;
});