<?php
/**
 * kts, 2014
 * User: ozhegov
 * Date: 04.05.14
 * Time: 15:19
 */

$roles = array(
    User::ROLE_USER => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Пользователь',
        'children' => array(
            'guest',
            'addProjectChain',
            'editProjectChain'
        ),
        'bizRule' => null,
        'data' => null
    ),
    User::ROLE_ADMIN => array(
        'type' => CAuthItem::TYPE_ROLE,
        'children' => array(
            'user',
            'accessUsers',
            'tasksCrud'
        ),
        'description' => 'Администратор',
        'bizRule' => null,
        'data' => null
    ),
);


$access = array(
    'accessUsers' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => '',
        'data' => null,
        'bizRule' => null,
    ),
    'tasksCrud' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => '',
        'data' => null,
        'bizRule' => null,
    )
);

return array_merge_recursive($roles, $access);