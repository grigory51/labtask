<?php

class AuthController extends Controller
{
    public function actionIndex()
    {
        if ($this->getUser()->isAuthorized()) {
            $this->redirect(array('site/index'));
        }

        $this->render('index');
    }

    public function actionLogin()
    {
        $email = $this->getRequest()->getParam('email');
        $password = $this->getRequest()->getParam('password');
        $identity = new UserIdentity($email, $password);

        if ($identity->authenticate()) {
            $this->getUser()->login($identity);
            $this->redirect($this->getUser()->returnUrl);
        } else {
            $this->render('index', array(
                'authError' => $identity->errorMessage
            ));
        }
    }

    public function actionLogout()
    {
        $this->validateCsrfOnGetRequest();
        $this->getUser()->logout();
        $this->redirect(array('site/index'));
    }
}