<?php

class SiteController extends Controller
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        $criteria->condition = '1=1';
        if ($this->getUser()->getRole() == User::ROLE_USER) {
            $criteria->condition .= ' AND assigned_id = :assignedId';
            $criteria->params = array(
                'assignedId' => $this->getUser()->getId()
            );
        }

        $tasks = Task::model()->findAll($criteria);
        $this->render('index', array(
			'tasks' => $tasks
        ));
    }

    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                $this->setJsonHeader();
                echo json_encode(AjaxHelper::composeUnsuccessfulAjaxResponse($error['message']));
            } else {
                $this->render('error', array(
                    'error' => $error
                ));
            }
        }
    }
}