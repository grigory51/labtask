<?php

/**
 * kts, 2013
 * User: ozhegov
 * Date: 29.11.13
 * Time: 3:55
 */
class UsersController extends Controller
{
    public function actionIndex()
    {
        $users = User::model()->findAll();
        $this->render('index', array(
            'users' => $users
        ));
    }

    public function actionPut()
    {
        $id = $this->getRequest()->getParam('id', false);

        if ($id) {
            $user = User::model()->resetScope()->findByPk($id);
            if (!$user) {
                throw new CHttpException(404, 'Пользователь не найден');
            }
        } else {
            $user = new User();
        }

        if ($this->getRequest()->isPostRequest) {
            $email = $this->getRequest()->getParam('email');
            $password = $this->getRequest()->getParam('password', false);
            $name = $this->getRequest()->getParam('name');
            $surname = $this->getRequest()->getParam('surname');
            $role = $this->getRequest()->getParam('role');

            $user->setEmail($email);
            $user->setName($name);
            $user->setSurname($surname);
            $user->setRole($role);

            if ($password) {
                $user->setPassword($password);
            }

            if ($user->save()) {
                $this->redirect(array('users/index'));
            } else {
                SessionHelper::SaveErrorsAndRedirect($this, $user);
            }
        }

        $errors = SessionHelper::GetErrorsAndInvalidObject($this);
        if ($errors) {
            $user = CHtml::value($errors, 'object', $user);
            $errors = CHtml::value($errors, 'errors', false);
        }

        return $this->render('form', array(
            'user' => $user,
            'errors' => $errors
        ));
    }

    public function actionDelete()
    {
        $this->validateCsrfOnGetRequest();

        $id = $this->getRequest()->getParam('id');
        $user = User::model()->findByPk($id);

        if (is_null($user)) {
            throw new CHttpException(404, 'User not found');
        }

        if ($user->delete()) {
            $this->redirect(array('users/index'));
        } else {
            throw new CHttpException(500, 'Ошибка при удалении пользователя');
        }
    }

}