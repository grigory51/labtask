<?php

/**
 * kts, 2015
 * User: ozhegov
 * Date: 23/10/15
 * Time: 18:17
 */
class TasksController extends Controller
{
    public function actionView()
    {
        $id = $this->getRequest()->getParam('id');

        $task = Task::model()->findByPk($id);
        $this->render('view', array(
            'task' => $task
        ));
    }

    public function actionPut()
    {
        $id = $this->getRequest()->getParam('id', false);

        if ($id) {
            $task = Task::model()->findByPk($id);
            if (!$task) {
                throw new CHttpException(404, 'Задача не найдена');
            }
        } else {
            $task = new Task();
        }

        if ($this->getRequest()->isPostRequest) {
            $title = $this->getRequest()->getParam('title');
            $description = $this->getRequest()->getParam('description');
            $assignedId = $this->getRequest()->getParam('assigned_id');
            $deadline = $this->getRequest()->getParam('deadline');

            $task->setTitle($title);
            $task->setDescription($description);
            $task->setAssignedId($assignedId);
            $task->setCreateTime(date('Y-m-d H:i:s'));
            $task->setDeadline($deadline);
            $task->setCreatorId($this->getUser()->getId());

            if ($task->save()) {
                $this->redirect(array('site/index'));
            } else {
                SessionHelper::SaveErrorsAndRedirect($this, $task);
            }
        }

        $errors = SessionHelper::GetErrorsAndInvalidObject($this);
        if ($errors) {
            $task = CHtml::value($errors, 'object', $task);
            $errors = CHtml::value($errors, 'errors', false);
        }

        $this->render('form', array(
            'task' => $task,
            'users' => User::model()->findAll(),
            'errors' => $errors
        ));
    }

    public function actionDelete()
    {
        $this->validateCsrfOnGetRequest();

        $id = $this->getRequest()->getParam('id');
        $task = Task::model()->findByPk($id);

        if (is_null($task)) {
            throw new CHttpException(404, 'User not found');
        }

        if ($task->delete()) {
            $this->redirect(array('site/index'));
        } else {
            throw new CHttpException(500, 'Ошибка при удалении задачи');
        }
    }

    public function actionClose()
    {
        $id = $this->getRequest()->getParam('id');

        if ($this->getRequest()->isPostRequest) {
            /**@var Task $task */
            $task = Task::model()->find('id=:id AND status!="closed"', array(
                'id' => $id,
            ));

            if (!$task) {
                throw new CHttpException(404);
            }

            $task->setStatus('close');
            $task->save();
        }

        $this->redirect(array('tasks/view', 'id' => $id));
    }

    public function actionOnReview()
    {
        $id = $this->getRequest()->getParam('id');

        if ($this->getRequest()->isPostRequest) {
            /**@var Task $task */
            $task = Task::model()->find('id=:id AND status="open" AND assigned_id = :assignedId', array(
                'id' => $id,
                'assignedId' => $this->getUser()->getId()
            ));

            if (!$task) {
                throw new CHttpException(404);
            }

            $task->setStatus('in_review');
            $task->save();
        }

        $this->redirect(array('tasks/view', 'id' => $id));
    }
}