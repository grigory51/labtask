<?php


class UserIdentity extends CUserIdentity
{
    /* Те роли пользователей, которым можно логиниться в админку*/
    protected $allowRoleLogin
        = array(
            User::ROLE_ADMIN,
            User::ROLE_USER
        );

    private $_id;
    public $errorMessage;

    public function getId() { return $this->_id; }

    public function getName() { return $this->username; }

    public function authenticate()
    {
        $this->errorMessage = 'Неверный адрес электронной почты или пароль';
        $this->errorCode = self::ERROR_USERNAME_INVALID;
        /**@var User $user */
        $user = User::model()->find(array(
            'condition' => 'email=:email',
            'params' => array(':email' => $this->username)
        ));

        if ($user &&
            CPasswordHelper::verifyPassword($this->password, $user->password) &&
            in_array($user->getRole(), $this->allowRoleLogin)
        ) {
            $this->_id = $user->getId();
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

}