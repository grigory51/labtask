<?php

class Controller extends BaseController
{
    public $layout = 'main';

    public function filters()
    {
        return array(
            'accessControl'
        );
    }

    public function accessRules()
    {
        return array(
            // Можно ходить всем
            array('allow',
                'controllers' => array('auth'),
            ),
            array('allow',
                'controllers' => array('site'),
                'actions' => array('error'),
            ),

            // Можно ходить всем залогинившимся
            array('allow',
                'controllers' => array('site'),
                'users' => array('@')
            ),
            array('allow',
                'controllers' => array('tasks'),
                'actions' => array('view', 'onReview'),
                'users' => array('@')
            ),

            // По ролям
            array('allow',
                'controllers' => array('users'),
                'roles' => array('accessUsers'),
            ),
            array('allow',
                'controllers' => array('tasks'),
                'actions' => array('put', 'delete', 'close'),
                'roles' => array('tasksCrud')
            ),

            // Остальное все запрещаем
            array('deny',
                'users' => array('*')
            )
        );
    }
}