<?php

/*
 * jQuery File Upload Plugin PHP Class 7.0.1
 * https://github.com/blueimp/jQuery-File-Upload
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *
 * Redesign by grigory51
 */

class UploadHandler
{
    protected $options;
    protected $uploadFiles = array();

    function __construct($options = null)
    {
        $this->options = array(
            'upload_dir' => dirname(__FILE__) . '/files/',
            'upload_url' => '',
            'mkdir_mode' => 0755,
            'param_name' => 'files',
            // Read files in chunks to avoid memory limits when download_via_php
            // is enabled, set to 0 to disable chunked reading of files:
            'readfile_chunk_size' => 10 * 1024 * 1024, // 10 MiB
            // Defines which files can be displayed inline when downloaded:
            'file_types' => '/\.(gif|jpe?g|png)$/i',
            // The php.ini settings upload_max_filesize and post_max_size
            // take precedence over the following max_file_size setting:
            'max_file_size' => false,
            'min_file_size' => 1,
            // The maximum number of files for the upload directory:
            'max_number_of_files' => null,

            'max_width' => null,
            'max_height' => null,
            'min_width' => 1,
            'min_height' => 1,
            // Set the following option to false to enable resumable uploads:
            'discard_aborted_uploads' => true,
        );
        if (!is_null($options)) {
            $this->options = array_replace_recursive($this->options, $options);
        }
    }

    /**
     * @return CHttpRequest
     */
    protected function getRequest()
    {
        return Yii::app()->getRequest();
    }

    protected function getServerVar($id)
    {
        return CHtml::value($_SERVER, $id, false);
    }

    protected function getOption($name)
    {
        if (!isset($this->options[$name])) {
            throw new UploadException(UploadException::ERROR_UNKNOWN_OPTION, $name);
        }
        return CHtml::value($this->options, $name, false);
    }

    public function processUpload()
    {
        if ($this->getRequest()->isPostRequest) {
            return $this->post();
        } else {
            throw new UploadException(UploadException::ERROR_METHOD_NOT_ALLOWED);
        }
    }

    protected function getUploadPath($fileName = null)
    {
        $fileName = $fileName ? $fileName : '';
        return realpath($this->getOption('upload_dir')) . '/' . $fileName;
    }

    protected function getDownloadUrl($fileName)
    {
        return $this->getOption('upload_url') . rawurlencode($fileName);
    }

    protected function getFileSize($filePath, $clearStatCache = false)
    {
        if ($clearStatCache) {
            clearstatcache(true, $filePath);
        }
        return filesize($filePath);
    }

    protected function isValidFileObject($fileName)
    {
        $filePath = $this->getUploadPath($fileName);
        if (is_file($filePath) && $fileName[0] !== '.') {
            return true;
        }
        return false;
    }

    protected function getFileObject($fileName)
    {
        if ($this->isValidFileObject($fileName)) {
            $file = new stdClass();
            $file->name = $fileName;
            $file->size = $this->getFileSize($this->getUploadPath($fileName));
            $file->url = $this->getDownloadUrl($file->name);
            return $file;
        }
        return null;
    }

    function getConfigBytes($val)
    {
        $val = trim($val);
        $last = strtolower($val[strlen($val) - 1]);
        switch ($last) {
            case 'g':
                $val *= 1024 * 1024 * 1024;
                break;
            case 'm':
                $val *= 1024 * 1024;
                break;
            case 'k':
                $val *= 1024;
        }
        return $val;
    }

    protected function validate($uploadedFile, $file, $isSafe = false)
    {
        $postMaxSize = $this->getConfigBytes(ini_get('post_max_size'));
        $contentLength = intval($this->getServerVar('CONTENT_LENGTH'));
        $maxFileSize = $this->getOption('max_file_size');
        $minFileSize = $this->getOption('min_file_size');

        if ($uploadedFile && (is_uploaded_file($uploadedFile) || $isSafe)) {
            $fileSize = $this->getFileSize($uploadedFile);
        } else {
            $fileSize = $contentLength;
        }

        if ($postMaxSize && ($contentLength > $postMaxSize)) {
            throw new UploadException(UploadException::ERROR_POST_MAX_SIZE);
        }

        if (!preg_match($this->getOption('file_types'), $file->name)) {
            throw new UploadException(UploadException::ERROR_NOT_ALLOWED_TYPE);
        }
        if ($maxFileSize && ($fileSize > $maxFileSize || $file->size > $maxFileSize)) {
            new UploadException(UploadException::ERROR_TOO_BIG);
        }
        if ($minFileSize && $fileSize < $minFileSize) {
            new UploadException(UploadException::ERROR_TOO_SMALL);
        }

        return true;
    }

    protected function getUniqueFilename($name, $contentRange = null)
    {
        function hashName($name)
        {
            $extension = strtolower(pathinfo($name, PATHINFO_EXTENSION));
            $hashedName = sha1($name . microtime() . rand(1, 1000));
            return substr($hashedName, 0, 2) . '/' . substr($hashedName, 2, 2) . '/' . substr($hashedName, 4, 2) . '/' . $hashedName . '.' . $extension;
        }

        $name = hashName($name);
        while (is_dir($this->getUploadPath($name))) {
            $name = hashName($name);
        }
        // Keep an existing filename if this is part of a chunked upload:
        $uploadedBytes = intval($contentRange[1]);
        while (is_file($this->getUploadPath($name))) {
            if ($uploadedBytes === $this->getFileSize($this->getUploadPath($name))) {
                break;
            }
            $name = hashName($name);
        }
        return $name;
    }


    protected function getFileName($name, $type = null, $contentRange = null)
    {
        // Remove path information and dots around the filename, to prevent uploading
        // into different directories or replacing hidden system files.
        // Also remove control characters and spaces (\x00..\x20) around the filename:
        $name = trim(basename(stripslashes($name)), ".\x00..\x20");

        if (!$name) {
            $name = str_replace('.', '-', microtime(true));
        }
        // Add missing file extension for known image types:
        if (strpos($name, '.') === false && preg_match('/^image\/(gif|jpe?g|png)/', $type, $matches)) {
            $name .= '.' . $matches[1];
        }
        return $this->getUniqueFilename($name, $contentRange);
    }

    protected function handleFileUpload($uploadedFile, $name, $size, $type, $error, $index = null, $contentRange = null, $isSafe = false)
    {
        $file = new stdClass();
        $file->name = $this->getFileName($name, $type, $contentRange);
        $file->size = intval($size);
        $file->type = $type;

        if ($this->validate($uploadedFile, $file, $error, $index, $isSafe)) {
            $uploadDir = $this->getUploadPath();
            $this->makeDir($uploadDir);
            $filePath = $this->getUploadPath($file->name);


            $appendFile = $contentRange && is_file($filePath) && $file->size > $this->getFileSize($filePath);
            if ($uploadedFile) {
                // multipart/formdata uploads (POST method uploads)
                if ($appendFile) {
                    file_put_contents($filePath, fopen($uploadedFile, 'r'), FILE_APPEND);
                } else {
                    $isUploaded = is_uploaded_file($uploadedFile);

                    if ($isUploaded || $isSafe) {
                        $this->makeDir($filePath);
                    }

                    if ($isUploaded) {
                        move_uploaded_file($uploadedFile, $filePath);
                    } elseif ($isSafe) {
                        copy($uploadedFile, $filePath);
                    }
                }
            } else {
                // Non-multipart uploads (PUT method support)
                file_put_contents($filePath, fopen('php://input', 'r'), $appendFile ? FILE_APPEND : 0);
            }
            $fileSize = $this->getFileSize($filePath, $appendFile);
            if ($fileSize === $file->size) {
                $file->url = $this->getDownloadUrl($file->name);
            } else {
                $file->size = $fileSize;
                if (!$contentRange && $this->getOption('discard_aborted_uploads')) {
                    unlink($filePath);
                    $file->error = 'abort';
                }
            }
        }
        return $file;
    }

    protected function readfile($filePath)
    {
        $fileSize = $this->getFileSize($filePath);
        $chunkSize = $this->getOption('readfile_chunk_size');
        if ($chunkSize && $fileSize > $chunkSize) {
            $handle = fopen($filePath, 'rb');
            while (!feof($handle)) {
                echo fread($handle, $chunkSize);
                ob_flush();
                flush();
            }
            fclose($handle);
            return $fileSize;
        }
        return readfile($filePath);
    }

    protected function header($title, $value = null)
    {
        if (is_null($value)) {
            header("{$title}");
        } else {
            header("{$title}: {$value}");
        }
    }

    protected function getVersionParam()
    {
        return isset($_GET['version']) ? basename(stripslashes($_GET['version'])) : null;
    }

    protected function getSingularParamName()
    {
        return substr($this->options['param_name'], 0, -1);
    }

    protected function getFileNameParam()
    {
        $name = $this->getSingularParamName();
        return isset($_GET[$name]) ? basename(stripslashes($_GET[$name])) : null;
    }

    protected function getFileNamesParams()
    {
        $params = CHtml::value($_GET, $this->getOption('param_name'), array());
        foreach ($params as $key => $value) {
            $params[$key] = basename(stripslashes($value));
        }
        return $params;
    }

    protected function getFileType($filePath)
    {
        switch (strtolower(pathinfo($filePath, PATHINFO_EXTENSION))) {
            case 'jpeg':
            case 'jpg':
                return 'image/jpeg';
            case 'png':
                return 'image/png';
            case 'gif':
                return 'image/gif';
            default:
                return '';
        }
    }


    public function post()
    {
        $upload = CHtml::value($_FILES, $this->getOption('param_name'), null);
        $fileName = $this->getServerVar('HTTP_CONTENT_DISPOSITION') ? rawurldecode(preg_replace('/(^[^"]+")|("$)/', '', $this->getServerVar('HTTP_CONTENT_DISPOSITION'))) : null;
        // Parse the Content-Range header, which has the following form:
        // Content-Range: bytes 0-524287/2000000
        $contentRange = $this->getServerVar('HTTP_CONTENT_RANGE') ? preg_split('/[^0-9]+/', $this->getServerVar('HTTP_CONTENT_RANGE')) : null;
        $size = $contentRange ? $contentRange[3] : null;
        $files = array();
        if ($upload && is_array($upload['tmp_name'])) {
            // param_name is an array identifier like "files[]",
            // $_FILES is a multi-dimensional array:
            foreach ($upload['tmp_name'] as $index => $value) {
                $files[] = $this->handleFileUpload(
                    $upload['tmp_name'][$index],
                    $fileName ? $fileName : $upload['name'][$index],
                    $size ? $size : $upload['size'][$index],
                    $upload['type'][$index],
                    $upload['error'][$index],
                    $index,
                    $contentRange
                );
            }
        } else {
            // param_name is a single object identifier like "file",
            // $_FILES is a one-dimensional array:

            $files = $this->handleFileUpload(
                isset($upload['tmp_name']) ? $upload['tmp_name'] : null,
                $fileName ? $fileName : (isset($upload['name']) ? $upload['name'] : null),
                $size ? $size : (isset($upload['size']) ? $upload['size'] : $this->getServerVar('CONTENT_LENGTH')),
                isset($upload['type']) ? $upload['type'] : $this->getServerVar('CONTENT_TYPE'),
                isset($upload['error']) ? $upload['error'] : null,
                null,
                $contentRange
            );
        }

        return array($this->getOption('param_name') => $files);
    }

    public function staticUpload($localFileName, $originalFileName, $fileSize, $imageType)
    {
        return $this->handleFileUpload($localFileName, $originalFileName, $fileSize, $imageType, 0, 0, NULL, true);
    }

    private function makeDir($filePath)
    {
        $fileDir = pathinfo($filePath, PATHINFO_DIRNAME);
        if (!is_dir($fileDir)) {
            if (!@mkdir($fileDir, $this->getOption('mkdir_mode'), true)) {
                throw new UploadException(UploadException::ERROR_STORAGE_RIGHTS);
            }
        }
    }
}

class UploadException extends CException
{
    const ERROR_UPLOAD_MAX_FILESIZE = 'upload_max_filesize';
    const ERROR_POST_MAX_SIZE = 'post_max_size';
    const ERROR_TOO_BIG = 'max_file_size';
    const ERROR_TOO_SMALL = 'min_file_size';
    const ERROR_METHOD_NOT_ALLOWED = 'method_not_allowed';
    const ERROR_NOT_ALLOWED_TYPE = 'accept_file_types';
    const ERROR_MAX_FILES_NUMBER = 'max_number_of_files';
    const ERROR_FAIL_WRITE = 'fail_write';
    const ERROR_UNKNOWN = 'unknown_error';
    const ERROR_STORAGE_RIGHTS = 'storage_rights';
    const ERROR_UNKNOWN_OPTION = 'unknown_option';

    private $errorType;

    private $errorMessages
        = array(
            self::ERROR_UPLOAD_MAX_FILESIZE => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
            self::ERROR_POST_MAX_SIZE => 'The uploaded file exceeds the post_max_size directive in php.ini',
            self::ERROR_TOO_BIG => 'File is too big',
            self::ERROR_TOO_SMALL => 'File is too small',
            self::ERROR_NOT_ALLOWED_TYPE => 'Filetype not allowed',
            self::ERROR_MAX_FILES_NUMBER => 'Maximum number of files exceeded',
            self::ERROR_FAIL_WRITE => 'Failed to write file to disk',
            self::ERROR_UNKNOWN => 'Unknown error',
            self::ERROR_STORAGE_RIGHTS => 'Not enough folder rights',
            self::ERROR_METHOD_NOT_ALLOWED => 'Method not allowed',
            self::ERROR_UNKNOWN_OPTION => 'Unknown option'
        );

    public function __construct($errorType, $detail = false, $code = 0)
    {
        $this->errorType = $errorType;
        $message = CHtml::value($this->errorMessages, $errorType, '');
        if ($detail) {
            $message .= (': ' . $detail);
        }
        parent::__construct($message, $code);
    }
}