<?php

/**
 * kts, 2014
 * User: ozhegov
 * Date: 22.01.14
 * Time: 4:25
 */
class GeoHelper
{
    public static function getUserIP()
    {
        return CHtml::value($_SERVER, 'REMOTE_ADDR', 0);
    }
}