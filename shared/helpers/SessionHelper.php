<?php

/**
 * kts, 2014
 * User: ozhegov
 * Date: 01.05.14
 * Time: 2:03
 */
class SessionHelper
{
    /**
     * @param BaseController $controller
     * @param BaseModel|array $entity
     * @param array $additionalErrors
     */
    public static function SaveErrorsAndRedirect($controller, $entity = null, $additionalErrors = array())
    {
        if (!is_array($additionalErrors)) {
            $additionalErrors = array($additionalErrors);
        }

        $currentValue = self::GetFromSession($controller->getRequest()->getUrl());
        $errorMessage = array(
            'object' => $entity,
            'errors' => array_merge($entity instanceof CActiveRecord ? $entity->getErrors() : array(), $additionalErrors)
        );

        if (empty($currentValue)) {
            self::SetToSession($controller->getRequest()->getUrl(), array('errors' => $errorMessage));
        } else {
            $currentValue['errors'] = $errorMessage;
            self::SetToSession($controller->getRequest()->getUrl(), $currentValue);
        }

        $response = array($controller->getRequest()->getUrl());
        $controller->redirect($response);
    }

    /**
     * @param BaseController $controller
     * @param array $messages
     */
    public static function SaveSuccessAndRedirect($controller, $messages = array())
    {
        if (!is_array($messages)) {
            $messages = array($messages);
        }

        $currentValue = self::GetFromSession($controller->getRequest()->getUrl());

        if (empty($currentValue)) {
            self::SetToSession($controller->getRequest()->getUrl(), array('success' => $messages));
        } else {
            $currentValue['success'] = $messages;
            self::SetToSession($controller->getRequest()->getUrl(), $currentValue);
        }

        $controller->redirect(array($controller->getRequest()->getUrl()));
    }

    /**
     * @param BaseController $controller
     * @return null|array
     */
    public static function GetErrorsAndInvalidObject($controller)
    {
        $controllerObjects = self::GetFromSession($controller->getRequest()->getUrl());
        if (is_array($controllerObjects)) {
            $errors = CHtml::value($controllerObjects, 'errors');
            if ($errors) {
                unset($controllerObjects['errors']);
                self::SetToSession($controller->getRequest()->getUrl(), $controllerObjects);
            }
            return $errors;
        } else {
            return null;
        }
    }

    /**
     * @param BaseController $controller
     * @return null|array
     */
    public static function GetSuccess($controller)
    {
        $controllerObjects = self::GetFromSession($controller->getRequest()->getUrl());
        if (is_array($controllerObjects)) {
            $success = CHtml::value($controllerObjects, 'success', false);
            if ($success) {
                unset($controllerObjects['success']);
                self::SetToSession($controller->getRequest()->getUrl(), $controllerObjects);
            }
            return $success;
        } else {
            return null;
        }
    }

    public static function SetToSession($key, $value)
    {
        Yii::app()->session[$key] = array($value);
    }

    public static function GetFromSession($key, $default = null)
    {
        $value = Yii::app()->session[$key];
        if (is_array($value)) {
            return reset($value);
        } else {
            return $default;
        }
    }

    public static function ClearSessionValueByKey($key)
    {
        unset(Yii::app()->session[$key]);
    }
}