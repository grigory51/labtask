<?php

/**
 * kts, 2013
 * User: penyaev
 * Date: 10.12.13
 * Time: 21:35
 */
class StringsHelper
{
    /**
     * Склонятор.
     * @param $iNumber int числительное, по которому склоняем
     * @param $nameNominative string есть кто, что - место
     * @param $nameGenitive string нет кого, чего - места
     * @param $namePlural string много кого, чего - мест
     * @return string
     */
    public static function pluralize($iNumber, $nameNominative, $nameGenitive, $namePlural) {
        $t1 = $iNumber % 10;
        $t2 = $iNumber % 100;
        if ($t1 == 1 && $t2 != 11) {
            /* 1, 21, 31... */
            return $nameNominative;
        } elseif ($t1 >= 2 && $t1 <= 4 && ($t2 < 10 || $t2 >= 20)) {
            /* 2-4, 22-24, 32-34... */
            return $nameGenitive;
        } else {
            return $namePlural;
        }
    }

    public static function nl2p($string) {
        return '<p>'.implode('</p><p>', array_reduce(explode("\n", $string), function ($acc, $string) {
            $string = trim($string);
            if (!empty($string))
                $acc []= $string;
            return $acc;
        }, array())).'</p>';
    }

    public static function getMonthName($monthNumber, $case = 1)
    {
        $monthsNames = array(
            1 => array(
                1 => 'Январь',
                2 => 'Января',
            ),
            2 => array(
                1 => 'Февраль',
                2 => 'Февраля',
            ),
            3 => array(
                1 => 'Март',
                2 => 'Марта',
            ),
            4 => array(
                1 => 'Апрель',
                2 => 'Апреля',
            ),
            5 => array(
                1 => 'Май',
                2 => 'Мая',
            ),
            6 => array(
                1 => 'Июнь',
                2 => 'Июня',
            ),
            7 => array(
                1 => 'Июль',
                2 => 'Июля',
            ),
            8 => array(
                1 => 'Август',
                2 => 'Августа',
            ),
            9 => array(
                1 => 'Сентябрь',
                2 => 'Сентября',
            ),
            10 => array(
                1 => 'Октябрь',
                2 => 'Октября',
            ),
            11 => array(
                1 => 'Ноябрь',
                2 => 'Ноября',
            ),
            12 => array(
                1 => 'Декабрь',
                2 => 'Декабря',
            ),
        );

        return CHtml::value($monthsNames, $monthNumber . '.' . $case);
    }

    public static function getHumanFriendlyDate($timestamp = null, $withYear = false)
    {
        if (is_null($timestamp)) {
            $timestamp = time();
        } else {
            if (is_string($timestamp)) {
                $timestamp = strtotime($timestamp);
            }
        }
        $result = date('j', $timestamp) . ' ' . mb_strtolower(self::getMonthName(intval(date('n', $timestamp)), 2), 'UTF-8');
        if ($withYear) {
            $result .= ' ' . date('Y', $timestamp);
        }
        return $result;
    }

    public static function getHumanFriendlyDateInterval($timestampFrom, $timestampTo)
    {
        if (empty($timestampFrom) && empty($timestampTo)) {
            return '';
        }

        if (empty($timestampFrom)) {
            $prefix = 'до ';
            $timestampFrom = $timestampTo;
        } elseif (empty($timestampTo)) {
            $prefix = 'с ';
            $timestampTo = $timestampFrom;
        } else {
            $prefix = '';
        }

        $monthFrom = date('n', $timestampFrom);
        $monthTo = date('n', $timestampTo);

        $return = $prefix . date('j', $timestampFrom);

        if (!empty($monthFrom) && !empty($monthTo)) {
            if ($monthFrom != $monthTo) {
                $return .= ' ' . mb_strtolower(self::getMonthName(date('n', $timestampFrom), 2), 'UTF-8');
                $return .= '–' . date('j', $timestampTo);
            } else {
                if (date('j', $timestampFrom) != date('j', $timestampTo)) // убираем случаи типа "23-23 января"
                {
                    $return .= '–' . date('j', $timestampTo);
                }
            }
        }

        $return .= ' ' . mb_strtolower(self::getMonthName(date('n', $timestampTo), 2), 'UTF-8');

        return $return;
    }

    public static function transliterate($string)
    {
        if (is_null($string)) {
            $string = '';
        }

        $mapping = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
            'ь' => '\'', 'ы' => 'y', 'ъ' => '\'',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
            'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '\'',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
            ' ' => '_',
        );

        return trim(preg_replace('/[^-a-z0-9_]+/u', '', strtolower(strtr($string, $mapping))), '-_');
    }

    /**
     * Возвращает первое предложение текста
     * @param $text
     * @param int $minimumLength Минимальная длина предложения.
     * @return string
     */
    public static function getFirstSentence($text, $minimumLength = 60)
    {
        $firstSentence = '';
        $description = $text;
        if (mb_strlen($text) < $minimumLength) {
            return $text;
        }

        $delimiterPositions = array(strlen($description));

        $delimiterPosition = strpos($description, '!', $minimumLength);
        if (!empty($delimiterPosition)) {
            $delimiterPositions[] = $delimiterPosition;
        }

        $delimiterPosition = strpos($description, '?', $minimumLength);
        if (!empty($delimiterPosition)) {
            $delimiterPositions[] = $delimiterPosition;
        }

        $delimiterPosition = strpos($description, '.', $minimumLength);
        if (!empty($delimiterPosition)) {
            $delimiterPositions[] = $delimiterPosition;
        }

        if (!empty($delimiterPositions)) {
            $delimiterPosition = min($delimiterPositions);
            if ($delimiterPosition) {
                $firstSentence = substr($description, 0, $delimiterPosition) . '.';
            }
        }

        return $firstSentence;
    }

    public static function uppercaseFirstLetter($string)
    {
        return mb_strtoupper(mb_substr($string, 0, 1)) . mb_substr($string, 1);
    }
} 