<?php

/**
 * kts, 2013
 * User: penyaev
 * Date: 22.11.13
 * Time: 0:49
 */
class BaseController extends CController
{
    public $returnUrl;
    public $csrfToken;
    public $pageDescription;
    public $mainImage;

    protected $requireJsConfigPath = '';
    protected $requireJsConfigName = 'main';

    public function getRequireJsConfig() { return "{$this->requireJsConfigPath}/{$this->requireJsConfigName}"; }

    public function getRequireJsConfigName() { return $this->requireJsConfigName; }

    public function isMainPage() { return $this->getRoute() == 'site/index'; }

    public function isDebug() { return YII_DEBUG; }

    public function init()
    {
        parent::init();
        $this->returnUrl = $this->getRequest()->getParam('returnUrl');
        if (!$this->returnUrl) {
            $this->returnUrl = Yii::app()->request->url;
        }
        $this->csrfToken = $this->getRequest()->getParam('csrf_token');

        if (YII_DEBUG) {
            $showLogs = SessionHelper::GetFromSession('showLogs', true);
            if ($this->getRequest()->getParam('log_off', false) !== false) {
                $showLogs = false;
            }
            if ($this->getRequest()->getParam('log_on', false) !== false) {
                $showLogs = true;
            }
            if (!$showLogs) {
                $this->disableLogs();
            } else {
                $this->enableLogs();
            }
            SessionHelper::SetToSession('showLogs', $showLogs);
        }
    }

    protected function disableLogs() { $this->setEnabledToLogs(false); }

    protected function enableLogs() { $this->setEnabledToLogs(true); }

    private function setEnabledToLogs($enableValue)
    {
        foreach (Yii::app()->log->routes as $route) {
            $route->enabled = $enableValue;
        }
    }

    public function getCsrfToken() { return $this->getRequest()->getCsrfToken(); }

    public function getCsrfTokenName() { return $this->getRequest()->csrfTokenName; }

    public function isCountersEnable() { return CHtml::value(Yii::app()->params, 'enableCounters', false); }

    /**
     * Если нужно совершить изменяющее действие (logout, удаление) через get-запрос, нужно в начале контроллера
     * обязательно вызвать этот метод. Он проверит переданный csrf-токен.
     * @throws CHttpException
     */
    public function validateCsrfOnGetRequest()
    {
        $token = $this->getRequest()->getQuery($this->getCsrfTokenName());
        if ($token !== $this->getCsrfToken()) {
            throw new CHttpException(400, 'Bad CSRF');
        }
    }

    /**
     * @return CHttpRequest
     */
    public function getRequest() { return Yii::app()->request; }

    public function getVersion() { return CHtml::value(Yii::app()->params, 'version'); }

    public function getVersionNumber()
    {
        $versionArray = explode(' ', $this->getVersion(), 2);
        return CHtml::value($versionArray, 0, 'unknown');
    }

    /**
     * Нужно для того, чтобы айдишник версии был слитным
     * @return string
     */
    public function getVersionHash() { return md5($this->getVersion()); }

    /**@return WebUser */
    public function getUser() { return Yii::app()->user; }

    protected function setJsonHeader() { header('Content-type: application/json; charset=UTF-8'); }

    protected function setPlainHeader() { header('Content-type: text/plain; charset=UTF-8'); }

    public function checkAccess($operation, $params = array()) { return $this->getUser()->checkAccess($operation, $params); }

    public function beginCache($id, $options = array())
    {
        $tags = CHtml::value($options, 'tags', array());
        $duration = intval(CHtml::value($options, 'duration', 12 * 3600)); //fixme по дефолту кеш на 12 часов
        $return = parent::beginCache($id, array(
            'dependency' => array(
                'class' => 'shared.memcache.Tags',
                'tags' => $tags,
            ),
            'duration' => $duration,
        ));
        return $return;
    }

    /**@return CDbConnection */
    public function getDb() { return Yii::app()->db; }

    public function setPageDescription($description) { $this->pageDescription = $description; }

    public function sendStatsMetric($metricName, $value) { return GraphiteHelper::send($metricName, $value); }

    public function setMainImage($mainImageUrl) { $this->mainImage = $mainImageUrl; }

    public function getUserIp() { return GeoHelper::getUserIP(); }

    protected function afterRender($view, &$output)
    {
        if (YII_DEBUG) {
            $logger = new CLogger();

            $executionTime = $logger->getExecutionTime() * 1000;
            $peakMemoryUsage = memory_get_peak_usage() / (1024 * 1024);

            list($queryCount, $queryTime) = $this->getDb()->getStats();
            $queryTime *= 1000;

            Yii::log('Execution time: ' . $executionTime . ' ms', $executionTime < 150 ? CLogger::LEVEL_INFO : CLogger::LEVEL_WARNING, 'application.performance');
            Yii::log('Peak memory usage: ' . $peakMemoryUsage . ' mb', $peakMemoryUsage < 32 ? CLogger::LEVEL_INFO : CLogger::LEVEL_WARNING, 'application.performance');
            Yii::log('Query count: ' . $queryCount, $queryCount < 10 ? CLogger::LEVEL_INFO : CLogger::LEVEL_WARNING, 'application.performance');
            Yii::log('Total query time ' . $queryTime . ' ms', CLogger::LEVEL_INFO, 'application.performance');
        }
    }

    /**
     * Для создания ссылок на статический контент
     * Url берет из params.urls.static
     * Подставляет версию статики
     * Поддерживает создание урлов различных форматов:
     * cdn.ktsstudio.ru/img/1.0.0/cat.png, cdn.ktsstudio.ru/img/cat.png?1.0.0, cdn.ktsstudio.ru/img/cat.png
     * @param $path
     * @param bool $disableVersion
     * @return string
     */
    public function createStaticUrl($path, $disableVersion = false)
    {
        $urlStatic = $this->getStaticUrl();
        if ($disableVersion) {
            return "{$urlStatic}{$path}";
        } else {
            if (CHtml::value(Yii::app()->params, 'versionInPath', false)) {
                return "{$urlStatic}{$this->getVersionNumber()}/{$path}";
            } else {
                return "{$urlStatic}{$path}?{$this->getVersionNumber()}";
            }
        }
    }

    public function getStaticUrl() { return CHtml::value(Yii::app()->params, 'urls.static', ''); }

    public function getCurrentUrl() { return $this->getRequest()->requestUri; }
} 