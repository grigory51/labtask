<?php

/**
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $assigned_id
 * @property integer $creator_id
 * @property string $create_time
 * @property string $status
 * @property string $deadline
 *
 * @property User $creator
 * @property User $assigned
 */
class Task extends BaseModel
{
    const STATUS_OPEN = 'open';
    const STATUS_IN_REVIEW = 'in_review';
    const STATUS_CLOSE = 'close';

    public function tableName() { return 'task'; }

    public function rules()
    {
        return array(
            array('title, creator_id, create_time', 'required'),
            array('assigned_id, creator_id', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 255),
            array('status', 'length', 'max' => 9),
            array('description, deadline', 'safe'),
        );
    }

    public function relations()
    {
        return array(
            'creator' => array(self::BELONGS_TO, 'User', 'creator_id'),
            'assigned' => array(self::BELONGS_TO, 'User', 'assigned_id'),
        );
    }

    public static function getListStatuses($codeOnly = false)
    {
        $roles = array(
            self::STATUS_OPEN => 'Открыта',
            self::STATUS_IN_REVIEW => 'На проверке',
            self::STATUS_CLOSE => 'Закрыта'
        );
        if ($codeOnly) {
            return array_keys($roles);
        } else {
            return $roles;
        }
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Название',
            'description' => 'Описание',
            'assigned_id' => 'Назначано',
            'creator_id' => 'Автор',
            'create_time' => 'Время создания',
            'status' => 'Статус',
            'deadline' => 'Срок выполнения',
        );
    }

    /**
     * @param string $className
     * @return Task
     */
    public static function model($className = __CLASS__) { return parent::model($className); }

    public function getTitle() { return $this->title; }

    public function setTitle($title) { $this->title = $title; }

    public function getDeadline() { return $this->deadline; }

    public function setDeadline($deadline) { $this->deadline = $deadline; }

    public function getDescription() { return $this->description; }

    public function setDescription($description) { $this->description = $description; }

    public function getAssignedId() { return $this->assigned_id; }

    public function setAssignedId($assigned_id) { $this->assigned_id = $assigned_id; }

    public function getCreatorId() { return $this->creator_id; }

    public function setCreatorId($creator_id) { $this->creator_id = $creator_id; }

    public function getCreateTime() { return $this->create_time; }

    public function setCreateTime($create_time) { $this->create_time = $create_time; }

    public function getStatus() { return $this->status; }

    public function getStatusTitle() { return CHtml::value($this->getListStatuses(), $this->getStatus()); }

    public function setStatus($status) { $this->status = $status; }

    /**
     * @return User
     */
    public function getCreator() { return $this->creator; }

    /**
     * @return User
     */
    public function getAssigned() { return $this->assigned; }
}
