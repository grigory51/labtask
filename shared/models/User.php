<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property string $surname
 * @property string $role
 */
class User extends BaseModel
{
    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';

    /**
     * Если пароль был изменен, то примет значение true
     * @var bool
     */
    private $isPasswordChanged = false;

    public static function getListRoles($codeOnly = false)
    {
        $roles = array(
            self::ROLE_ADMIN => 'Начальник',
            self::ROLE_USER => 'Сотрудник'
        );
        if ($codeOnly) {
            return array_keys($roles);
        } else {
            return $roles;
        }
    }

    public function tableName() { return 'user'; }

    public function rules()
    {
        $minPasswordLength = 6;
        $message = $minPasswordLength . ' ' . StringsHelper::pluralize($minPasswordLength, 'символ', 'символа', 'символов');

        return array(
            array('email', 'unique', 'message' => 'Эта электронная почта уже используется', 'caseSensitive' => true),
            array('email', 'email', 'message' => '{attribute} имеет неверный фромат'),
            array('password, name', 'required', 'message' => '{attribute} не может быть пустым'),
            array('email, surname, role', 'required', 'message' => '{attribute} не может быть пустой'),
            array('email, password, name, surname', 'length', 'tooLong' => 'Превышена максимальная длина', 'max' => 255),
            array('role', 'length', 'max' => 5),
            array('password', 'length', 'tooShort' => "Минимальная длина пароля — {$message}", 'min' => $minPasswordLength)
        );
    }

    public function relations() { return array(); }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'email' => 'Элекронная почта',
            'password' => 'Пароль',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'role' => 'Роль',
        );
    }

    public function beforeSave()
    {
        if ($this->isPasswordChanged) {
            $this->password = CPasswordHelper::hashPassword($this->password);
        }

        $this->surname = trim($this->surname);
        $this->name = trim($this->name);
        $this->email = trim($this->email);

        return true;
    }

    /**
     * @param string $className
     * @return User
     */
    public static function model($className = __CLASS__) { return parent::model($className); }

    public function getRole() { return $this->role; }

    public function getRoleTitle() { return CHtml::value($this->getListRoles(), $this->getRole()); }

    public function setRole($role) { $this->role = $role; }

    public function getEmail() { return $this->email; }

    public function setEmail($email) { $this->email = $email; }

    public function getName() { return $this->name; }

    public function setName($name) { $this->name = $name; }

    public function getPassword() { return $this->password; }

    public function setPassword($password) {
        $this->password = $password;
        $this->isPasswordChanged = true;
    }

    public function getSurname() { return $this->surname; }

    public function setSurname($surname) { $this->surname = $surname; }

    public function getFullName($reverse = false)
    {
        if ($reverse) {
            return "{$this->surname} {$this->name}";
        } else {
            return "{$this->name} {$this->surname}";
        }
    }
}
