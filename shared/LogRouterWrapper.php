<?php

/**
 * kts, 2014
 * User: ozhegov
 * Date: 22/09/14
 * Time: 20:29
 */
class LogRouterWrapper extends CLogRouter
{

    /**
     * Оборачивает вывод CWebLogRoute и CProfileLogRoute в <div class="yiiLogWrapper"></div>, чтобы логи были
     * читабельны при использовании Bootstrap
     */
    public function processLogs()
    {
        if (Yii::app()->getRequest()->isAjaxRequest) {
            parent::processLogs();
        } else {
            $logger = Yii::getLogger();
            $wrapperOn = false;
            /**@var CLogRoute[] $routes */
            $routes = $this->getRoutes();
            foreach ($routes as $route) {
                if ($route->enabled) {
                    if (!$wrapperOn && ($route instanceof CWebLogRoute || $route instanceof CProfileLogRoute)) {
                        echo '<div class="yiiLogWrapper">';
                        $wrapperOn = true;
                    }
                    $route->collectLogs($logger, true);
                }
            }
            if ($wrapperOn) {
                echo '</div>';
            }
        }
    }
}