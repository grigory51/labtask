<?php

/**
 * kts, 2013
 * User: ozhegov
 * Date: 28.11.13
 * Time: 8:05
 */
class WebUser extends CWebUser
{
    /** @var  $model User */
    protected $model = null;

    /**
     * @param $password
     * @return bool|string
     * @deprecated
     */
    public static function isPasswordInvalid($password)
    {
        if (mb_strlen($password) < User::MIN_PASS_LENGTH) {
            return 'Пароль не может быть короче ' . User::MIN_PASS_LENGTH . ' ';
        }
        return false;
    }

    /**
     * @return User
     */
    public function getModel()
    {
        if (!$this->isGuest && $this->model === null) {
            $this->model = User::model()->findByPk($this->getId());
        }
        return $this->model;
    }

    public function isAuthorized() { return !is_null($this->getModel()); }

    /**
     * @param bool $bReverse swap surname and name
     * @return bool|string
     */
    public function getFullName($bReverse = false)
    {
        return $this->getModel() ? $this->getModel()->getFullName($bReverse) : '';
    }

    public function getRole() { return $this->getModel() ? $this->getModel()->getRole() : false; }

    public function getPermissions() { return $this->getModel() ? $this->getModel()->getPermissions() : null; }

    public function isAdmin()
    {
        return $this->getRole() === User::ROLE_ADMIN;
    }
}