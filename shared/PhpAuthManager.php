<?php

/**
 * kts, 2014
 * User: ozhegov
 * Date: 04.05.14
 * Time: 15:29
 */
class PhpAuthManager extends CPhpAuthManager
{
    public function init()
    {
        if ($this->authFile === null) {
            throw new CException('Role file not found');
        }

        parent::init();

        if (Yii::app()->user->isAuthorized()) {
            $this->assign(Yii::app()->user->getRole(), Yii::app()->user->getId());
        }
    }
}