<?php

/**
 * kts, 2013
 * User: penyaev
 * Date: 22.11.13
 * Time: 1:27
 */
abstract class BaseModel extends CActiveRecord
{
    public function primaryKey() { return 'id'; }

    public function getId() { return $this->id; }
} 