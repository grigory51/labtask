<?php

$yii = dirname(__FILE__) . '/lib/yii/yii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';

$suppressError = isset($_SERVER['DISABLE_ERROR_SHOW']);
if (!$suppressError) {
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
} else {
    error_reporting(E_ALL ^ E_NOTICE);
    ini_set('display_errors', 'Off');
}

require_once($yii);
Yii::createWebApplication($config)->run();